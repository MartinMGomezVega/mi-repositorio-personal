// 
// Decompiled by Procyon v0.5.36
// 

package Servicios.Implementaciones;

import Modelos.Suelo;
import Modelos.Ambiente;
import Servicios.iAmbiente;

public class AmbienteServicio implements iAmbiente
{
    private Ambiente ambiente;
    
    public AmbienteServicio(final int filas, final int columnas) {
        this.ambiente = new Ambiente(filas, columnas);
    }
    
    @Override
    public void inicializarAmbiente() {
        for (int i = 0; i < this.ambiente.getAmbiente().length; ++i) {
            for (int j = 0; j < this.ambiente.getAmbiente()[0].length; ++j) {
                this.ambiente.getAmbiente()[i][j] = new Suelo();
            }
        }
        this.ubicarObstaculos();
        this.determinarBordes();
    }
    
    @Override
    public void ubicarObstaculos() {
        final double cantidadSuelo = this.ambiente.getAmbiente().length * this.ambiente.getAmbiente()[0].length;
        for (int i = 0; i < Math.random() * cantidadSuelo; ++i) {
            final int valorRandomFila = (int)(Math.random() * (this.ambiente.getAmbiente().length - 2.0) + 1.0);
            final int valorRandomColumna = (int)(Math.random() * (this.ambiente.getAmbiente().length - 2.0) + 1.0);
            this.ambiente.getAmbiente()[valorRandomFila][valorRandomColumna].setTieneObstaculo(true);
        }
    }
    
    @Override
    public void determinarBordes() {
        for (int i = 0; i < this.ambiente.getAmbiente().length; ++i) {
            this.ambiente.getAmbiente()[0][i].getBordes().replace("arriba", true);
            this.ambiente.getAmbiente()[this.ambiente.getAmbiente().length - 1][i].getBordes().replace("abajo", true);
        }
        for (int i = 0; i < this.ambiente.getAmbiente()[0].length; ++i) {
            this.ambiente.getAmbiente()[i][0].getBordes().replace("izquierda", true);
            this.ambiente.getAmbiente()[i][this.ambiente.getAmbiente()[0].length - 1].getBordes().replace("derecha", true);
        }
    }
    
    @Override
    public Suelo[][] obtenerAmbienteSuelo() {
        return this.ambiente.getAmbiente();
    }
    
    @Override
    public Ambiente getAmbiente() {
        return this.ambiente;
    }
}