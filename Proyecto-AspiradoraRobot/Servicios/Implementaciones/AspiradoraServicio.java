// 
// Decompiled by Procyon v0.5.36
// 

package Servicios.Implementaciones;

import Modelos.Suelo;
import Errores.CepillosSuciosError;
import Errores.SinBateriaError;
import Errores.AspiradoraApagadaError;
import Modelos.Ambiente;
import Modelos.Cepillo;
import Modelos.Aspiradora;
import Servicios.iAspiradora;

public class AspiradoraServicio implements iAspiradora
{
    Aspiradora aspiradora;
    
    public AspiradoraServicio() {
        this.aspiradora = new Aspiradora();
    }
    
    @Override
    public void inicializarCepillos() {
        this.aspiradora.getCepillos()[0] = new Cepillo();
        this.aspiradora.getCepillos()[1] = new Cepillo();
    }
    
    @Override
    public void cambiarVelocidad(final int velocidad) {
        this.aspiradora.setVelocidad(velocidad);
    }
    
    @Override
    public boolean limpiar(final Ambiente ambiente) {
        if (this.aspiradora.getBateria() > 0 && this.cepillosLimpios() && !this.terminoDeLimpiar(ambiente)) {
            ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].setEstaLimpio(true);
            this.ensuciarCepillos();
        }
        return ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getEstaLimpio();
    }
    
    @Override
    public void limpiarUltimaCelda(final Ambiente ambiente) {
        ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].setEstaLimpio(true);
        this.ensuciarCepillos();
    }
    
    @Override
    public boolean terminoDeLimpiar(final Ambiente ambiente) {
        if (ambiente.getAmbiente()[0].length % 2 == 0) {
            return ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("arriba") && ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("derecha");
        }
        return ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("abajo") && ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("derecha");
    }
    
    @Override
    public void mover(final Ambiente ambiente) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError {
        if (!this.aspiradora.getEstaPrendida()) {
            throw new AspiradoraApagadaError();
        }
        if (ambiente.getAmbiente()[0].length == this.aspiradora.getPosicionColumna() || !this.limpiar(ambiente)) {
            this.finalizarLimpieza(ambiente);
        }
        else {
            this.limpiar(ambiente);
            this.bajarBateria();
            if (this.aspiradora.getPosicionColumna() % 2 == 1) {
                this.moverArriba(ambiente);
            }
            else {
                this.moverAbajo(ambiente);
            }
        }
    }
    
    @Override
    public void encender() throws SinBateriaError {
        if (this.aspiradora.getBateria() == 0) {
            throw new SinBateriaError();
        }
        this.aspiradora.setEstaPrendida(true);
    }
    
    @Override
    public void moverArriba(final Ambiente ambiente) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError {
        if (!ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("arriba")) {
            final Suelo[][] ambiente2 = ambiente.getAmbiente();
            final int posicionFila = this.aspiradora.getPosicionFila();
            final Aspiradora aspiradora = this.aspiradora;
            if (ambiente2[posicionFila - Aspiradora.getPosMover()][this.aspiradora.getPosicionColumna()].getTieneObstaculo()) {
                final Aspiradora aspiradora2 = this.aspiradora;
                final int posicionFila2 = this.aspiradora.getPosicionFila();
                final Aspiradora aspiradora3 = this.aspiradora;
                aspiradora2.setPosicionFila(posicionFila2 - Aspiradora.getPosEsquivar());
                this.mover(ambiente);
            }
            else {
                final Aspiradora aspiradora4 = this.aspiradora;
                final int posicionFila3 = this.aspiradora.getPosicionFila();
                final Aspiradora aspiradora5 = this.aspiradora;
                aspiradora4.setPosicionFila(posicionFila3 - Aspiradora.getPosMover());
                this.mover(ambiente);
            }
        }
        else {
            this.aspiradora.setPosicionColumna(this.aspiradora.getPosicionColumna() + 1);
            this.mover(ambiente);
        }
    }
    
    @Override
    public void moverAbajo(final Ambiente ambiente) throws SinBateriaError, CepillosSuciosError, AspiradoraApagadaError {
        if (!ambiente.getAmbiente()[this.aspiradora.getPosicionFila()][this.aspiradora.getPosicionColumna()].getBordes().get("abajo")) {
            final Suelo[][] ambiente2 = ambiente.getAmbiente();
            final int posicionFila = this.aspiradora.getPosicionFila();
            final Aspiradora aspiradora = this.aspiradora;
            if (ambiente2[posicionFila + Aspiradora.getPosMover()][this.aspiradora.getPosicionColumna()].getTieneObstaculo()) {
                final Aspiradora aspiradora2 = this.aspiradora;
                final int posicionFila2 = this.aspiradora.getPosicionFila();
                final Aspiradora aspiradora3 = this.aspiradora;
                aspiradora2.setPosicionFila(posicionFila2 + Aspiradora.getPosEsquivar());
                this.mover(ambiente);
            }
            else {
                final Aspiradora aspiradora4 = this.aspiradora;
                final int posicionFila3 = this.aspiradora.getPosicionFila();
                final Aspiradora aspiradora5 = this.aspiradora;
                aspiradora4.setPosicionFila(posicionFila3 + Aspiradora.getPosMover());
                this.mover(ambiente);
            }
        }
        else {
            this.aspiradora.setPosicionColumna(this.aspiradora.getPosicionColumna() + 1);
            this.mover(ambiente);
        }
    }
    
    @Override
    public void finalizarLimpieza(final Ambiente ambiente) throws CepillosSuciosError, SinBateriaError {
        this.limpiarUltimaCelda(ambiente);
        this.aspiradora.setEstaPrendida(false);
        this.determinarError();
        System.out.println("El ambiente ha sido limpiado correctamente");
    }
    
    @Override
    public void determinarError() throws SinBateriaError, CepillosSuciosError {
        if (this.aspiradora.getBateria() == 0) {
            throw new SinBateriaError();
        }
        if (!this.cepillosLimpios()) {
            throw new CepillosSuciosError();
        }
    }
    
    @Override
    public void bajarBateria() {
        this.aspiradora.setBateria(this.aspiradora.getBateria() - 1);
    }
    
    @Override
    public void ensuciarCepillos() {
        this.aspiradora.getCepillos()[0].ensuciarCepillo();
        this.aspiradora.getCepillos()[1].ensuciarCepillo();
    }
    
    @Override
    public boolean cepillosLimpios() {
        return this.aspiradora.getCepillos()[0].getEstaLimpio() && this.aspiradora.getCepillos()[1].getEstaLimpio();
    }
    
    @Override
    public Aspiradora getAspiradora() {
        return this.aspiradora;
    }
}