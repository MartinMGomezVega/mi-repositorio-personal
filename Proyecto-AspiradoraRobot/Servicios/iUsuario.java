// 
// Decompiled by Procyon v0.5.36
// 

package Servicios;

import Errores.AspiradoraApagadaError;
import Errores.SinBateriaError;
import Errores.CepillosSuciosError;

public interface iUsuario
{
    int cargarFilas();
    
    int cargarColumnas();
    
    int cargarVelocidad();
    
    String readInput();
    
    void mostrarAmbiente();
    
    void comenzarALimpiar() throws CepillosSuciosError, SinBateriaError, AspiradoraApagadaError;
}