// 
// Decompiled by Procyon v0.5.36
// 

package Errores;

public class AspiradoraApagadaError extends Exception
{
    public AspiradoraApagadaError() {
        super("La aspiradora esta apagada. Por favor enci\u00e9ndala para comenzar la limpieza");
    }
}