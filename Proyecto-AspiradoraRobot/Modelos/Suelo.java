// 
// Decompiled by Procyon v0.5.36
// 

package Modelos;

import java.util.HashMap;
import java.util.Map;

public class Suelo
{
    private boolean tieneObstaculo;
    private boolean estaLimpio;
    private Map<String, Boolean> bordes;
    
    public Suelo() {
        this.tieneObstaculo = false;
        this.estaLimpio = false;
        this.bordes = new HashMap<String, Boolean>();
        this.inicializarBordes();
    }
    
    public void inicializarBordes() {
        this.bordes.put("derecha", false);
        this.bordes.put("izquierda", false);
        this.bordes.put("arriba", false);
        this.bordes.put("abajo", false);
    }
    
    public void mostrarSuelo() {
        if (this.tieneObstaculo) {
            System.out.print("X");
        }
        else if (this.estaLimpio) {
            System.out.print("L");
        }
        else if (!this.estaLimpio) {
            System.out.print("S");
        }
    }
    
    public boolean getTieneObstaculo() {
        return this.tieneObstaculo;
    }
    
    public void setTieneObstaculo(final boolean tieneObstaculo) {
        this.tieneObstaculo = tieneObstaculo;
    }
    
    public boolean getEstaLimpio() {
        return this.estaLimpio;
    }
    
    public void setEstaLimpio(final boolean estaLimpio) {
        this.estaLimpio = estaLimpio;
    }
    
    public Map<String, Boolean> getBordes() {
        return this.bordes;
    }
    
    public void setBordes(final Map<String, Boolean> bordes) {
        this.bordes = bordes;
    }
}