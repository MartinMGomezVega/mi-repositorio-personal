// 
// Decompiled by Procyon v0.5.36
// 

package Modelos;

public class Cepillo
{
    private boolean estaLimpio;
    int suciedad;
    
    public Cepillo() {
        this.estaLimpio = true;
        this.suciedad = 0;
    }
    
    public void ensuciarCepillo() {
        ++this.suciedad;
        if (this.suciedad >= 100) {
            this.estaLimpio = false;
        }
    }
    
    public boolean getEstaLimpio() {
        return this.estaLimpio;
    }
    
    public void setEstaLimpio(final boolean estaLimpio) {
        this.estaLimpio = estaLimpio;
    }
    
    public int getSuciedad() {
        return this.suciedad;
    }
    
    public void setSuciedad(final int suciedad) {
        this.suciedad = suciedad;
    }
}