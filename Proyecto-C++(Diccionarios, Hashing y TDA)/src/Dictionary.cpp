#include "Dictionary.h"
#include <string>
#include "Key.h"
#include "Item.h"
#include <iostream>

using std::string;
using namespace std;
using UndavDictionary::Dictionary;
using UndavDictionary::UndavDictionaryIterator::DictionaryIterator;
using UndavKey::Key;
using UndavItem::Item;
const int DefaultCapacity = 4;

struct UndavDictionary::Dictionary{
	Key  **keys;
	Item **items;
	int quantityItems;
	int capacity;
};

struct Node{
	void* item;
	Node* next;
};

void Resize(Dictionary* dictionary, int newCapacity);
void Insert(Dictionary *dictionary, Key *key, UndavItem::Item *item,int ranking);
void Extract(Dictionary *dictionary, int ranking);
int GetRank(Key** keys, Key* keySearched, int startIndex, int endIndex);

Dictionary* UndavDictionary::Create(){
	Dictionary* newDictionary = new Dictionary;
	newDictionary->keys = new Key*[DefaultCapacity];
	newDictionary->items = new Item*[DefaultCapacity];
	newDictionary->quantityItems = 0;
	newDictionary->capacity = DefaultCapacity;
	return newDictionary;
}

void UndavDictionary::Put(Dictionary* dictionary, Key* key, UndavItem::Item* item){
	if (dictionary->quantityItems == 0){
		dictionary->keys[0]= key;
		dictionary->items[0]= item;
		dictionary->quantityItems ++;
	}else{
		int ranking = GetRank(dictionary->keys, key, 0,dictionary->quantityItems-1);
		if((ranking<dictionary->quantityItems) && (UndavKey::AreEquals(dictionary->keys[ranking],key))){
			dictionary->items[ranking]= item;
		}else{
			if(dictionary->quantityItems == dictionary->capacity){
				Resize(dictionary, 2 * dictionary->capacity);
			}
			Insert(dictionary, key, item, ranking);
			dictionary->quantityItems++;
		}
	}
}

Item* UndavDictionary::Get(const Dictionary* dictionary, Key* key){
	if (dictionary->quantityItems != 0){
		int ranking = GetRank(dictionary->keys, key, 0, dictionary->quantityItems -1);
		if((ranking<dictionary->quantityItems) && (UndavKey::AreEquals(dictionary->keys[ranking], key))){
			return dictionary->items[ranking];
		}
	}
	return NULL;
}

void Remove(Dictionary* dictionary, Key* key){
	if(!UndavDictionary::IsEmpty(dictionary)){
		int ranking = GetRank(dictionary->keys, key, 0, dictionary->quantityItems);
		if(UndavKey::AreEquals(dictionary->keys[ranking],key)){
			Extract(dictionary, ranking);
			if(dictionary->quantityItems == dictionary->capacity / 4){
				Resize(dictionary, dictionary->capacity / 2);
			}
		}
	}
}

bool UndavDictionary::Contains(const Dictionary* dictionary, Key* key){
	int ranking = GetRank(dictionary->keys, key, 0,dictionary->quantityItems -1);
	return((ranking<dictionary->quantityItems)&&(UndavKey::AreEquals(dictionary->keys[ranking],key)));
}

bool UndavDictionary::IsEmpty(const Dictionary* dictionary){
	return dictionary->quantityItems == 0;
}

int UndavDictionary::Count(const Dictionary* dictionary){
	return dictionary->quantityItems;
}

void UndavDictionary::Destroy(Dictionary* dictionary){
	delete [] dictionary->items;
	delete [] dictionary->keys;
	delete dictionary;
}

struct UndavDictionary::UndavDictionaryIterator::DictionaryIterator{
	Dictionary* dictionary;
	int index;
};

DictionaryIterator* UndavDictionary::UndavDictionaryIterator::Begin(Dictionary* dictionary){
	DictionaryIterator* iterator = new DictionaryIterator;
	iterator->index = 0;
	iterator->dictionary = dictionary;
	return iterator;
}
bool UndavDictionary::UndavDictionaryIterator::IsEnd(const DictionaryIterator* iterator){
	return iterator->index >= iterator->dictionary->quantityItems;
}
Key* UndavDictionary::UndavDictionaryIterator::GetKey(DictionaryIterator* iterator){
	if(!IsEmpty(iterator->dictionary) && iterator->index < iterator->dictionary->quantityItems)
		return iterator->dictionary->keys[iterator->index];
	return NULL;
}
Item* UndavDictionary::UndavDictionaryIterator::GetValue(DictionaryIterator* iterator){
	if(!IsEmpty(iterator->dictionary) && iterator->index < iterator->dictionary->quantityItems)
		return iterator->dictionary->items[iterator->index];
	return NULL;
}
void UndavDictionary::UndavDictionaryIterator::Next(DictionaryIterator* iterator){
	if(!IsEmpty(iterator->dictionary) && iterator->index < iterator->dictionary->quantityItems){
		iterator->index++;
	}
}
void UndavDictionary::UndavDictionaryIterator::Destroy(DictionaryIterator* iterator){
	if(iterator != NULL){
		delete iterator;
	}
}
void Resize(Dictionary* dictionary, int newCapacity){
	Key** newKeys= new Key*[newCapacity];
	Item** newItems= new Item*[newCapacity];
	for(int i = 0; i<dictionary->quantityItems;i++){
		newKeys[i]= dictionary->keys[i];
		newItems[i]= dictionary->items[i];
	}
	delete [] dictionary->keys;
	delete [] dictionary->items;
	dictionary->keys = newKeys;
	dictionary->items= newItems;
	dictionary->capacity = newCapacity;
}
void Insert(Dictionary *dictionary, Key *key, UndavItem::Item *item, int ranking) {
	for (int i = dictionary->quantityItems; i > ranking; i--) {
		dictionary->keys[i] = dictionary->keys[i - 1];
		dictionary->items[i] = dictionary->items[i - 1];
	}
	dictionary->keys[ranking] = key;
	dictionary->items[ranking] = item;
}
void Extract(Dictionary *dictionary, int ranking){
	for (int i = ranking; i < dictionary->quantityItems - 1; i++) {
		dictionary->keys[i] = dictionary->keys[i + 1];
		dictionary->items[i] = dictionary->items[i + 1];
	}
	dictionary->quantityItems--;
	dictionary->keys[dictionary->quantityItems] = NULL;
	dictionary->items[dictionary->quantityItems] = NULL;
}

int GetRank(Key** keys, Key* keySearched, int startIndex, int endIndex){
	while(startIndex <= endIndex){
		int middleIndex = startIndex +  ((endIndex - startIndex) /2);
		if(UndavKey::AreEquals(keys[middleIndex], keySearched)) return middleIndex;
		else
			UndavKey::IsLess(keySearched,keys[middleIndex]) ? endIndex = middleIndex-1 : startIndex = middleIndex+1;
	}
	return startIndex;
}
