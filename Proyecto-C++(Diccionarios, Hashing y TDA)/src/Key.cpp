#include "Key.h"
#include <string>
#include "Hashing.h"
using UndavKey::Key;
using std::string;
using UndavHashing::Hash;
using UndavKey::AreEquals;

int CompareKeysInt(void* keyA,void* keyB);
int CompareKeysString(void* keyA,void* keyB);

int HashKeyString(void* hashKey);
int HashKeyInt(void* hashInt);

struct UndavKey::Key{
	void* innerKey;
	CompareKeysFunction compareKeys;
	int hash;
};

Key* UndavKey::Create(string key){
	string* newInnerKey = new string;
	*((string*)newInnerKey) = key;
	return UndavKey::Create(newInnerKey, CompareKeysString, HashKeyString);
}

Key* UndavKey::Create(int key){
	int* newInnerKey = new int;
	*((int*)newInnerKey)= key;
	return UndavKey::Create(newInnerKey, CompareKeysInt, HashKeyInt);
}

Key* UndavKey::Create(void* key, CompareKeysFunction compareFunction, HashKeyFunction hashKey){
	Key* newKey = new Key;
	newKey->innerKey = key;
	newKey->hash = hashKey(key);
	newKey->compareKeys = compareFunction;
	return newKey;
}

const void* UndavKey::Get(const Key* key){
	return key->innerKey;
}

int UndavKey::GetHash(const Key* key){
	return key->hash;
}

bool UndavKey::AreEquals(const Key* keyA, const Key* keyB){
	return (keyA->compareKeys(keyA->innerKey, keyB->innerKey) == 0);
}

bool UndavKey::IsLess(const Key* keyA, const Key* keyB){
	return (keyA->compareKeys(keyA->innerKey, keyB->innerKey) == - 1);
}

void UndavKey::Destroy(Key* key){
	delete key;
}

int HashKeyInt(void* hashInt){
	int hash = *((int*) hashInt);
	return UndavHashing::HashInteger(hash);
}

int HashKeyString(void* hashKey){
	string NewHashKey = *((string*) hashKey);
	return UndavHashing::Hash(NewHashKey);
}

int CompareKeysString(void* keyA,void* keyB){
	string stringA = *((string*) keyA);
	string stringB = *((string*) keyB);
	if(stringA == stringB){
		return 0;
	}else if(stringA < stringB){
		return -1;
	}
	return 1;
}

int CompareKeysInt(void* keyA,void* keyB){
	int intA = *((int*) keyA);
	int intB = *((int*) keyB);
	if(intA == intB){
		return 0;
	}else if(intA < intB){
		return -1;
	}
	return 1;
}
