#include "List.h"
#include "Item.h"

using UndavItem::Item;
using UndavList::List;
using UndavList::UndavListIterator::ListIterator;

struct Node{
	void* item;
	Node* next;
};

struct UndavList::List{
	Node* first;
	Node* last;
	int quantityItems;
};

void RemoveFirst(List* list);
void RemoveLast(List* list);
Node* CreateLastNode(void* item);
Node* CreateNode(void* item, Node* next);

List* UndavList::Create(){
	List* newList = new List;
	newList->first = newList->last = NULL;
	newList->quantityItems = 0;
	return newList;
}

void UndavList::PushBack(List* list, void* item){
	Node* newLastNode = CreateLastNode(item);
	if(UndavList::IsEmpty(list)){
		list->last = list->first = newLastNode;
	}else{
		list->last->next = newLastNode;
		list->last = newLastNode;
	}
	list->quantityItems ++;
}

void UndavList::PushFront(List* list, void* item){
	Node* newFirstNode = CreateNode(item, list->first);
	list->first = newFirstNode;
	if(list->last == NULL){
		list->last = list->first;
	}
	list->quantityItems ++;
}

bool UndavList::IsEmpty(const List* list){
	return list->first == NULL;
}

struct UndavList::UndavListIterator::ListIterator{
	Node* currentNode;
};

ListIterator* UndavList::UndavListIterator::Begin(List* list){
	ListIterator* newIterator = new ListIterator;
	newIterator->currentNode = list->first;

	return newIterator;
}

bool UndavList::UndavListIterator::IsEnd(const ListIterator* iterator){
	return iterator->currentNode == NULL;
}

void* UndavList::UndavListIterator::GetItem(ListIterator* iterator){
	return iterator->currentNode == NULL ? NULL :iterator->currentNode->item;
}

void UndavList::UndavListIterator::Next(ListIterator* iterator){
	if(!UndavList::UndavListIterator::IsEnd(iterator)){
		iterator->currentNode = iterator->currentNode->next;
	}
}

void UndavList::UndavListIterator::Destroy(ListIterator* iterator){
	delete iterator;
}

void UndavList::Remove(List* list, UndavListIterator::ListIterator* position){
	if(position->currentNode == list->first){
		RemoveFirst(list);
	}else if(position->currentNode == list->last){
		RemoveLast(list);
	}else{
		Node* nodeToRemove = position->currentNode;
		Node* iterator = list->first;
		Node* previous = NULL;
		while(iterator!=nodeToRemove){
			previous = iterator;
			iterator = iterator->next;
		}
		previous->next = nodeToRemove->next;
		list->quantityItems--;
		delete nodeToRemove;
	}
}

void UndavList::Destroy(List* list){
	Node* iterator = list->first;
	while(iterator != NULL){
		Node* auxiliar = iterator;
		iterator = iterator->next;
		delete auxiliar;
	}
	delete list;
}

void RemoveFirst(List* list){
	if(!UndavList::IsEmpty(list)){
		Node* nodeToRemove = list->first;
		list->first = nodeToRemove->next;
		list->quantityItems--;
		if(list->quantityItems == 0){
			list->last = NULL;
		}
		delete nodeToRemove;
	}
}

void RemoveLast(List* list){
	if(!UndavList::IsEmpty(list)){
		if(list->quantityItems == 1){
			RemoveFirst(list);
		}else{
			Node* nodeToRemove = list->last;
			Node* iterator = list->first;
			while(iterator->next !=NULL){
				iterator = iterator->next;
			}
			iterator->next = NULL;
			list->quantityItems--;
			delete nodeToRemove;
		}
	}
}

Node* CreateNode(void* item, Node* next){
	Node* newNode = new Node;
	newNode->item = item;
	newNode->next = next;
	return newNode;
}

Node* CreateLastNode(void* item){
	return CreateNode(item, NULL);
}
