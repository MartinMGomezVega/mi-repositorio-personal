#include "Item.h"
#include <string>
using std::string;

using UndavItem::Item;

struct UndavItem::Item{
	void* value;
};

Item* UndavItem::Create(void* item){
	Item* newItem = new Item;
	newItem->value = item;
	return newItem;
}

void* UndavItem::Get(const Item* item){
	return item->value;
}

void UndavItem::Destroy(Item* item){
	delete item;
}
