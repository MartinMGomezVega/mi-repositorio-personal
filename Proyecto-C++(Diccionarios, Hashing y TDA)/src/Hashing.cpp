#include "Hashing.h"
#include <string>
using std::string;
using std::to_string;

const int M = 63973;

// Hashing Universal. Horner's method
int UndavHashing::Hash(string hashMe){
	int hash = 0;
	int coefficientA = 31415;
	int coefficientB = 27183;
	int tope = hashMe.size();
	for (int i=0;i<tope; i++,coefficientA = coefficientA*coefficientB %(M-1)){
		hash= (coefficientA * hash + hashMe[i])%M;
	}
	return hash;
}

int UndavHashing::HashInteger(int hashMe){
	string hashString = to_string(hashMe);
	return UndavHashing::Hash(hashString);

}
